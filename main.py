"""Solve the Python Challenge!"""
import solutions
import pkgutil
import logging


LOGGER = logging.getLogger(__name__)
INITIAL_URL = 'http://www.pythonchallenge.com/pc/def/0.html'


def main():
    """Go through the files in the solutions folder seeding each
    solution with the answer to the previous solution"""
    url = INITIAL_URL
    for module_tuple in sorted(
        list(pkgutil.iter_modules(solutions.__path__)),
        key=lambda mod: int(mod[1])
    ):

        problem_number_str = module_tuple[1]
        module = module_tuple[0].find_module(
            problem_number_str
        ).load_module(problem_number_str)
        next_url = module.solve(url)
        LOGGER.info('%s %s',  problem_number_str, next_url)
        url = next_url


if __name__ == '__main__':
    logging.basicConfig(
        format="%(asctime)s %(levelname).1s %(name)-16s %(message)s",
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.DEBUG
    )
    main()
