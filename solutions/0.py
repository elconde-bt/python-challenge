import urllib2
import re
import solutions


def solve(url):
    """This one is easy!"""
    for line in urllib2.urlopen(
        '{}/{}.html'.format(solutions.ROOT_URL, str(2**38))
    ).read().splitlines():
        match = re.search('URL=(.*).html', line)
        if not match:
            continue
        return '{}/{}.html'.format(solutions.ROOT_URL, match.groups()[0])
