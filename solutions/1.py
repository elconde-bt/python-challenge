import urllib2
import string
import logging
import solutions

LOGGER = logging.getLogger(__name__)


def unscramble(secret_message):
    """Unscramble the secret message"""
    letters = string.lowercase
    trans = string.maketrans(letters, letters[2:] + letters[:2])
    return string.translate(secret_message, trans)


def solve(url):
    found_color = False
    for line in urllib2.urlopen(url).read().splitlines():
        if 'font color="#f000f0"' in line:
            found_color = True
            continue
        if not found_color:
            continue
        secret_message = line
        break
    else:
        assert False, 'Cannot find secret message'
    LOGGER.debug(secret_message)
    LOGGER.debug(unscramble(secret_message))
    url_split = url.split('/')
    return '{}/{}.html'.format(
        solutions.ROOT_URL, unscramble(url_split[-1].split('.')[0])
    )
