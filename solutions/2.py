import logging
import urllib2
import string
import solutions


LOGGER = logging.getLogger(__name__)


def solve(url):
    ready = False
    secret_message = ''
    for line in urllib2.urlopen(url).read().splitlines():
        if not ready and 'find rare characters' in line:
            ready = True
            continue
        if not ready:
            continue
        for character in line:
            if character not in string.letters+' ':
                continue
            secret_message += character
    return '{}/{}.html'.format(solutions.ROOT_URL, secret_message)
