import logging
import urllib2
import re
import solutions


LOGGER = logging.getLogger(__name__)
RE_SOLUTION = re.compile(
    '[^A-Z][A-Z][A-Z][A-Z][a-z][A-Z][A-Z][A-Z][^A-Z]'
)


def solve(url):
    ready = False
    secret_message = ''
    for line in urllib2.urlopen(url).read().splitlines():
        if not ready and '<!--' in line:
            ready = True
            continue
        if not ready:
            continue
        matches = RE_SOLUTION.findall(line)
        if matches:
            LOGGER.debug(matches)
        for match in matches:
            secret_message += match[4]
    return '{}/{}.html'.format(solutions.ROOT_URL, secret_message)
